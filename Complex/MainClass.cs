﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Complex a = new Complex(3,5);
            Complex b = new Complex(2,4);
            Complex c = new Complex(0, 5);
            Complex d = new Complex(2, 0);
            Console.WriteLine("Число 1");
            Console.WriteLine(a);
            Console.WriteLine("Число 2");
            Console.WriteLine(b);
            Console.WriteLine("Число 3");
            Console.WriteLine(c);
            Console.WriteLine("Число 4");
            Console.WriteLine(d);
            Complex e = a + b;
            Console.WriteLine("Число 1 + Число 2");
            Console.WriteLine(e);
            Complex f = a + b;
            Console.WriteLine("Число 1 - Число 2");
            Console.WriteLine(f);
            Complex h = a * b;
            Console.WriteLine("Число 1 * Число 2");
            Console.WriteLine(h);
            Complex i = a * 2;
            Console.WriteLine("Число 1 * 2");
            Console.WriteLine(i);
            Complex g = a / 2;
            Console.WriteLine("Число 1 / 2");
            Console.WriteLine(g);
            Complex k = a + 2;
            Console.WriteLine("Число 1 + 2");
            Console.WriteLine(k);
            Complex l = a - 2;
            Console.WriteLine("Число 1 - 2");
            Console.WriteLine(l);
            Console.ReadKey();
        }
    }
}
